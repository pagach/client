import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Calendar from '@/components/Calendar/Index'
import DashboardIndex from '@/components/Dashboard/Index'
import ReportsIndex from '@/components/Reports/Index'
import SettingsIndex from '@/components/Settings/Index'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'dashboad',
      component: DashboardIndex
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/calendar',
      name: 'calendar',
      component: Calendar
    },
    {
      path: '/reports',
      name: 'reports',
      component: ReportsIndex
    },
    {
      path: '/settings',
      name: 'settings',
      component: SettingsIndex
    }
  ]
})
