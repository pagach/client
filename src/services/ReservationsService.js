import Api from '@/services/Api'

export default {
  fetchDayReservations (id, params) {
    return Api().get('api/v1/reservations/business/' + id, {
      params: params
    })
  },
  fetchUpcomingReservations (id, params) {
    return Api().get('api/v1/reservations/upcoming/' + id, {
      params: params
    })
  },
  createReservation (params) {
    return Api().post('entity/narucise_entity', params)
  }
}
