import Api from '@/services/Api'

export default {
  businessLogin (params) {
    return Api().get('api/v1/businesslogin', {
      params: params
    })
  },
  businessRefreshUser (payload) {
    return Api().get('api/v1/business', payload)
  },
  businessTypes (params) {
    return Api().get('api/v1/businesstypes')
  },
  businessUpdate (id, payload) {
    return Api().patch('admin/structure/narucise_entity/' + id, payload)
  }
}
